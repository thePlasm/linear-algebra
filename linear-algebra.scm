#lang racket

(provide pad-row gauss-elim valid-row)

(define
  (pad-row lst cols)
  (if
    (= cols 0)
    lst
    (pad-row
      (cons 0 lst)
      (- cols 1))))

(define
  (pad-cols matrix rows cols)
  (if
    (= rows 0)
    matrix
    (pad-cols
      (cons (pad-row '() cols) matrix)
      (- rows 1)
      cols)))

(define
  (find-pivotrow matrix index)
  (cond
    ((null? matrix) -1)
    ((= (caar matrix) 0)
     (find-pivotrow
       (cdr matrix)
       (+ index 1)))
    (else index)))

(define
  (get-index lst i)
  (cond
    ((null? lst) '())
    ((= i 0) (car lst))
    (else
      (get-index
	(cdr lst)
	(- i 1)))))

(define
  (remove-first matrix acc)
  (if
    (null? matrix)
    acc
    (remove-first
      (cdr matrix)
      (cons
	(cdar matrix)
	acc))))

(define
  (sub-row row f pivot acc)
  (if
    (null? row)
    acc
    (sub-row
      (cdr row)
      f
      (cdr pivot)
      (cons
	(-
	  (car row)
	  (*
	    f
	    (car pivot)))
	acc))))

(define
  (first-zero matrix pivotrow acc)
  (if
    (null? matrix)
    acc
    (first-zero
      (cdr matrix)
      pivotrow
      (cons
	(cons
	  0
          (reverse
           (sub-row
            (cdar matrix)
            (/
             (caar matrix)
             (car pivotrow))
            (cdr pivotrow)
            '())))
	acc))))

(define
  (join x y)
  (cond
    ((null? x)
     y)
    ((null? y)
     x)
    (else
      (join
	(cdr x)
	(cons (car x) y)))))

(define
  (replace index elem lst acc)
  (cond
    ((null? lst)
     '())
    ((= index 0)
     (join (cons elem acc) lst))
    (else
      (replace
	(- index 1)
	elem
	(cdr lst)
	(cons (car lst) acc)))))

(define
  (gauss-reduce matrix rows cols padcols reduced)
  (if
    (null? matrix)
    (pad-cols reduced rows cols)
    (let*
      ((pivot-index (find-pivotrow matrix 0))
       (pivotrow (get-index matrix pivot-index)))
      (gauss-reduce
      	(remove-first
	  (first-zero
	    (replace
	      pivot-index
	      (car matrix)
	      (cdr matrix)
	      '())
	    pivotrow
	    '())
	  '())
      	(- rows 1)
      	cols
	(+ padcols 1)
      	(cons (reverse (pad-row pivotrow padcols)) reduced)))))

(define
  (substitute val matrix acc)
  (if
    (null? matrix)
    acc
    (substitute
      val
      (cdr matrix)
      (cons
        (cons
          (-
            (caar matrix)
            (* (cadar matrix) val))
          (cddar matrix))
        acc))))

(define
  (first-nonzero lst)
  (cond
    ((null? lst) 0.0)
    ((or
      (= (car lst) 0)
      (= (car lst) +0.0)
      (= (car lst) -0.0))
     (first-nonzero (cdr lst)))
    (else (car lst))))

(define
  (gauss-solve matrix acc)
  (if
    (null? matrix)
    acc
    (let*
      ((denom
        (first-nonzero (cdar matrix)))
       (new-val
        (/
          (caar matrix)
            denom)))
      (if
       (= new-val +inf.0)
       (gauss-solve
         (cdr matrix)
         (cons +nan.f acc))
       (gauss-solve
         (reverse
           (substitute
             new-val
             (cdr matrix)
             '()))
         (cons
           new-val
           acc))))))

(define
  (gauss-elim matrix rows cols)
  (gauss-solve
    (gauss-reduce matrix rows cols 0 '())
    '()))

(define
  (lin-equiv lst1 lst2)
  (cond
    ((and
      (null? lst1)
      (null? lst2))
     #t)
    ((or
      (null? lst1)
      (null? lst2))
     #f)
    (else
      (letrec
        ((lin-eq-r
         (lambda
           (l1 l2)
           (cond
             ((and
               (null? l1)
               (null? l2))
              #t)
             ((or
                (null? l1)
                (null? l2))
              #f)
             ((=
                (car l2)
                (*
                  (car l1)
                  (/
                    (car lst2)
                    (car lst1))))
              (lin-eq-r
                (cdr l1)
                (cdr l2)))
             (else
              #f)))))
      (lin-eq-r
        (cdr lst1)
        (cdr lst2))))))

(define
  (valid-row row matrix)
  (cond
    ((null? matrix) #t)
    ((lin-equiv
       row
       (car matrix))
     (valid-row
       row
       (cdr matrix)))
    (else
     #f)))