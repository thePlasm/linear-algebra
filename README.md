# linear-algebra

A linear algebra library written in scheme.

# features

The gauss-elim function is for Gaussian elimination. The lin-equiv function will check if two rows are equivalent. The valid-row function will check if a row is equivalent to any rows in a matrix.
